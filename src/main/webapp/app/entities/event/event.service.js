(function() {
    'use strict';
    angular
        .module('teachernetApp')
        .factory('Event', Event);

    Event.$inject = ['$resource', 'DateUtils'];

    function Event ($resource, DateUtils) {
        var resourceUrl =  'api/events/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.eventDate = DateUtils.convertLocalDateFromServer(data.eventDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.eventDate = DateUtils.convertLocalDateToServer(data.eventDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.eventDate = DateUtils.convertLocalDateToServer(data.eventDate);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
