(function() {
    'use strict';

    angular
        .module('teachernetApp')
        .controller('SubjectDetailController', SubjectDetailController);

    SubjectDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Subject', 'Event', 'File'];

    function SubjectDetailController($scope, $rootScope, $stateParams, previousState, entity, Subject, Event, File) {
        var vm = this;

        vm.subject = entity;
        vm.previousState = previousState.name;

        vm.events = Event.query({subjectId: vm.subject.id});
        vm.files  = File.query({subjectId: vm.subject.id});

        if(vm.previousState === 'event.new' || vm.previousState === 'event.delete' || vm.previousState === 'event.edit'){
            vm.selectedMenu = 'EVENTS';
        } else {
            vm.selectedMenu = 'FILES';
        }


        var unsubscribe = $rootScope.$on('teachernetApp:subjectUpdate', function(event, result) {
            vm.subject = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
