(function() {
    'use strict';

    angular
        .module('teachernetApp')
        .controller('FileController', FileController);

    FileController.$inject = ['$scope', '$state', 'File'];

    function FileController ($scope, $state, File) {
        var vm = this;
        
        vm.files = [];

        loadAll();

        function loadAll() {
            File.query(function(result) {
                vm.files = result;
            });
        }
    }
})();
