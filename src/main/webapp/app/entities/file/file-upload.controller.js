(function() {
    'use strict';

    angular
        .module('teachernetApp')
        .controller('FileUploadController', FileUploadController);

    FileUploadController.$inject = ['$timeout', '$location', '$scope', '$http', '$stateParams', '$uibModalInstance', 'entity', 'File'];

    function FileUploadController ($timeout, $location, $scope, $http, $stateParams, $uibModalInstance, entity, File) {
        var vm = this;

        vm.file = entity;
        vm.clear = clear;
        vm.save = save;
        vm.currentSubjectId = $stateParams.id;
        vm.url = $location.absUrl();

        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });

        $scope.close = function(){
            $uibModalInstance.close();
        };

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            /*vm.isSaving = true;
            if (vm.file.id !== null) {
                File.update(vm.file, onSaveSuccess, onSaveError);
            } else {
                File.save(vm.file, onSaveSuccess, onSaveError);
            }*/
        }

        function onSaveSuccess (result) {
            $scope.$emit('teachernetApp:fileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

    }
})();
