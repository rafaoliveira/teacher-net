(function() {
    'use strict';

    angular
        .module('teachernetApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
