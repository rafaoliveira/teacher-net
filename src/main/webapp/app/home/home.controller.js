(function() {
    'use strict';

    angular
        .module('teachernetApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state'];

    function HomeController ($scope, Principal, LoginService, $state) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });

            checkRedirect();
        }
        function register () {
            $state.go('register');
        }

        function checkRedirect(){
            if (vm.isAuthenticated){
                $state.go('subject');
            }
        }
    }
})();
