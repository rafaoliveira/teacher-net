/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pitagoras.teachernet.web.rest.vm;
