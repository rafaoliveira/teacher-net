package com.pitagoras.teachernet.web.rest;

import java.io.*;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by RafaelOliveira on 03/12/2016.
 */
@Service
@Transactional
public class FileService {

    public String uploadFile(MultipartFile fileToUpload){
        String server = "www.rafaelmo.com.br";
        int port = 21;
        String user = "tn3tftp@rafaelmo.com.br";
        String pass = "ftp4s5";

        FTPClient ftpClient = new FTPClient();

        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // APPROACH #1: uploads first file using an InputStream

            String fileName = fileToUpload.getOriginalFilename();

            File toUpload = convert(fileToUpload);



            InputStream inputStream = new FileInputStream(toUpload);

            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(fileName, inputStream);
            inputStream.close();

            return fileName;

        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    public File convert(MultipartFile file)
    {
        try {
            File convFile = new File(file.getOriginalFilename());
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
            return convFile;
        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

}
