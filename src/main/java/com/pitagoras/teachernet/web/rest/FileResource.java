package com.pitagoras.teachernet.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pitagoras.teachernet.domain.File;
import com.pitagoras.teachernet.repository.FileRepository;
import com.pitagoras.teachernet.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;



/**
 * REST controller for managing File.
 */
@RestController
@RequestMapping("/api")
public class FileResource {

    private final Logger log = LoggerFactory.getLogger(FileResource.class);

    @Inject
    private FileRepository fileRepository;

    @Inject
    private FileService fileService;

    /**
     * POST  /files : Create a new file.
     *
     * @param file the file to create
     * @return the ResponseEntity with status 201 (Created) and with body the new file, or with status 400 (Bad Request) if the file has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/files",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<File> createFile(@Valid @RequestBody File file) throws URISyntaxException {
        log.debug("REST request to save File : {}", file);
        if (file.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("file", "idexists", "A new file cannot already have an ID")).body(null);
        }

        // TODO: codigo para efetivamente salvar o arquivo em um servidor FTP

        File result = fileRepository.save(file);
        return ResponseEntity.created(new URI("/api/files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("file", result.getId().toString()))
            .body(result);
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @Timed
    public ResponseEntity<Object> uploadFile(@RequestBody MultipartFile fileToUpload, @RequestParam String currentUrl, @RequestParam String subjectId) throws URISyntaxException {

        try {
            String storedFileName = fileService.uploadFile(fileToUpload);


            String fileDownloadURL = "http://rafaelmo.com.br/tnet/" + storedFileName;

            // store file data on database
            File newFile = new File();
            newFile.setFileName(storedFileName.substring(0, storedFileName.indexOf(".")));
            newFile.setSubjectId(Integer.valueOf(subjectId));
            newFile.setFileUrl(fileDownloadURL);
            fileRepository.save(newFile);


        } catch (Exception e){
            e.printStackTrace();
        }

        URI redirectURL = new URI(currentUrl.substring(0, currentUrl.indexOf("/#/")) + "/#/subject/" + subjectId);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(redirectURL);
        return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
    }



    /**
     * PUT  /files : Updates an existing file.
     *
     * @param file the file to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated file,
     * or with status 400 (Bad Request) if the file is not valid,
     * or with status 500 (Internal Server Error) if the file couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/files",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<File> updateFile(@Valid @RequestBody File file) throws URISyntaxException {
        log.debug("REST request to update File : {}", file);
        if (file.getId() == null) {
            return createFile(file);
        }
        File result = fileRepository.save(file);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("file", file.getId().toString()))
            .body(result);
    }

    /**
     * GET  /files : get all the files.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of files in body
     */
    @RequestMapping(value = "/files",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<File> getAllFiles(
        @RequestParam(value = "subjectId", required = true) Integer subjectId) {
        log.debug("REST request to get all Files of a Subject");
        List<File> files = fileRepository.findAllBySubjectId(subjectId);
        //List<File> files = fileRepository.findAll();
        return files;
    }

    /**
     * GET  /files/:id : get the "id" file.
     *
     * @param id the id of the file to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the file, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/files/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<File> getFile(@PathVariable Long id) {
        log.debug("REST request to get File : {}", id);
        File file = fileRepository.findOne(id);
        return Optional.ofNullable(file)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /files/:id : delete the "id" file.
     *
     * @param id the id of the file to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/files/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFile(@PathVariable Long id) {
        log.debug("REST request to delete File : {}", id);

        // TODO: código para efetivamente excluir arquivo do servidor FTP

        fileRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("file", id.toString())).build();
    }

}
