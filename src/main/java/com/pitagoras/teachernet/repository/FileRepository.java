package com.pitagoras.teachernet.repository;

import com.pitagoras.teachernet.domain.File;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the File entity.
 */
@SuppressWarnings("unused")
public interface FileRepository extends JpaRepository<File,Long> {
    List<File> findAllBySubjectId(Integer subjectId);
}
