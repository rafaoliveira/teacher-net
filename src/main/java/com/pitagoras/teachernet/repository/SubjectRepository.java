package com.pitagoras.teachernet.repository;

import com.pitagoras.teachernet.domain.Subject;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Subject entity.
 */
@SuppressWarnings("unused")
public interface SubjectRepository extends JpaRepository<Subject,Long> {
    List<Subject> findAllByUserId(Integer userId);
}
