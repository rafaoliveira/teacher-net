package com.pitagoras.teachernet.repository;

import com.pitagoras.teachernet.domain.Event;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Event entity.
 */
@SuppressWarnings("unused")
public interface EventRepository extends JpaRepository<Event,Long> {
    List<Event> findAllBySubjectId(Integer subjectId);
}
